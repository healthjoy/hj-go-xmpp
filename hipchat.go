package hipchat

import (
	"bitbucket.org/healthjoy/hj-go-xmpp/xmpp"
	"crypto/rand"
	"errors"
	"fmt"
	"io"
	"log"
	"regexp"
	"time"
)

var (
	Host           = "chat.hipchat.com"
	Conf           = "conf.hipchat.com"
	regexpImage, _ = regexp.Compile("<img src='([^']+)' title='([^']+)' longdesc='([^']*)##([^']*)'")
)

// A Client represents the connection between the application to the HipChat
// service.
type Client struct {
	Username string
	Password string
	Resource string
	Id       string

	OnReconnect chan bool

	// private
	mentionNames    map[string]string
	connection      *xmpp.Conn
	receivedUsers   chan []*User
	receivedRooms   chan []*Room
	ReceivedMessage chan *Message

	messageBuffer map[string]chan *Message

	alive    chan bool
	ClosedCh chan bool
}

// A Message represents a message received from HipChat.
type Message struct {
	From        string
	To          string
	Body        string
	MentionName string
	Stamp       time.Time
	Mid         string
	Attachments []*xmpp.Attachment
}

// A User represents a member of the HipChat service.
type User struct {
	Id          string
	Name        string
	MentionName string
}

// A Room represents a room in HipChat the Client can join to communicate with
// other members..
type Room struct {
	Id    string
	Name  string
	Owner string
	Topic string
}

// NewClient creates a new Client connection from the user name, password and
// resource passed to it.
func NewClient(user, pass, resource string) (*Client, error) {
	id := user + "@" + Host
	connection, err := xmpp.Dial(Host)

	c := &Client{
		Username: user,
		Password: pass,
		Resource: resource,
		Id:       id,

		// private
		connection:      connection,
		mentionNames:    make(map[string]string),
		receivedUsers:   make(chan []*User),
		receivedRooms:   make(chan []*Room, 10),
		ReceivedMessage: make(chan *Message, 20),
		OnReconnect:     make(chan bool),

		messageBuffer: make(map[string]chan *Message),

		alive:    make(chan bool),
		ClosedCh: make(chan bool, 1),
	}

	if err != nil {
		return c, err
	}

	err = c.authenticate()
	if err != nil {
		return c, err
	}

	go c.listen()
	go c.KeepAlive()
	return c, nil
}

// Messages returns a read-only channel of Message structs. After joining a
// room, messages will be sent on the channel.
func (c *Client) Messages() <-chan *Message {
	return c.ReceivedMessage
}

// Rooms returns an slice of Room structs.
func (c *Client) Rooms() []*Room {
	c.requestRooms()
	return <-c.receivedRooms
}

// Users returns a slice of User structs.
func (c *Client) Users() []*User {
	c.requestUsers()
	return <-c.receivedUsers
}

// Status sends a string to HipChat to indicate whether the client is available
// to chat, away or idle.
func (c *Client) Status(s string) {
	c.connection.Presence(c.Id, s)
}

// Join accepts the room id and the name used to display the client in the
// room.
func (c *Client) Join(roomId, resource string, history int) {
	c.connection.MUCPresence(roomId+"/"+resource, c.Id, history)
}

func (c *Client) Leave(roomId, resource string) {
	c.connection.MUCUnavailable(roomId+"/"+resource, c.Id)
}

// Say accepts a room id, the name of the client in the room, and the message
// body and sends the message to the HipChat room.
func (c *Client) Say(roomId, name, body string, attachments []xmpp.Attachment) string {
	return c.connection.MUCSend(roomId, c.Id+"/"+c.Resource, body, attachments)
}

// KeepAlive is meant to run as a goroutine. It sends a single whitespace
// character to HipChat every 60 seconds. This keeps the connection from
// idling after 150 seconds.
func (c *Client) KeepAlive() {
	for {
		select {
		case <-c.ClosedCh:
			log.Printf("%s XMPP closed", c.Id)
			c.ClosedCh <- true
			return
		case <-time.After(1 * time.Minute):
			isClosed := c.connection.KeepAlive()
			if isClosed {
				c.ClosedCh <- true
			}
		}
	}
}

func (c *Client) IsAlive() bool {
	select {
	case <-c.ClosedCh:
		c.ClosedCh <- true
		return false
	case <-time.After(100 * time.Millisecond):
		return true
	}
}

func (c *Client) requestRooms() {
	c.connection.Discover(c.Id, Conf)
}

func (c *Client) requestUsers() {
	c.connection.Roster(c.Id, Host)
}

func (c *Client) LoadHistory(roomJid string, start time.Time, limit int) []Message {
	queryId := id()
	c.messageBuffer[queryId] = make(chan *Message)
	c.connection.History(roomJid, start, limit, queryId)
	history := []Message{}

	for {
		select {
		case m := <-c.messageBuffer[queryId]:
			if m == nil {
				delete(c.messageBuffer, queryId)
				return history
			}
			history = append(history, *m)
		case <-time.After(15 * time.Second):
			delete(c.messageBuffer, queryId)
			return history
		}
	}
}

func (c *Client) authenticate() error {
	c.connection.Stream(c.Id, Host)
	for {
		element, err := c.connection.Next()
		if err != nil {
			return err
		}
		switch element.Name.Local + element.Name.Space {
		case "stream" + xmpp.NsStream:
			features := c.connection.Features()
			if features.StartTLS != nil {
				c.connection.StartTLS()
			} else {
				for _, m := range features.Mechanisms {
					if m == "PLAIN" {
						c.connection.Auth(c.Username, c.Password)
					}
				}
			}
		case "proceed" + xmpp.NsTLS:
			c.connection.UseTLS()
			c.connection.Stream(c.Id, Host)

		case "success" + xmpp.NsSASL:
			c.connection.Stream(c.Id, Host)
			c.connection.Bind(c.Resource)
			c.connection.Session()

		case "failure" + xmpp.NsSASL:
			return errors.New("could not authenticate")

		case "iq" + xmpp.NsJabberClient:
			for _, attr := range element.Attr {
				if attr.Name.Local == "type" && attr.Value == "result" {
					return nil // authenticated
				}
			}

			return errors.New("could not authenticate")
		}
	}

	return errors.New("unexpectedly ended auth loop")
}

func (c *Client) Close() {
	log.Println("Closing XMPP Client connection")

	c.connection.Close()

	close(c.ReceivedMessage)
	close(c.receivedRooms)
	close(c.receivedUsers)
}

func strtotime(str string) time.Time {
	stamp, err := time.Parse("2006-01-02T15:04:05Z", str)
	if err != nil {
		stamp = time.Now()
	}
	return stamp
}

func getAttachments(attachments []*xmpp.Attachment, htmlBody string) []*xmpp.Attachment {
	if htmlBody == "" {
		return nil
	}
	res := regexpImage.FindAllStringSubmatch(htmlBody, -1)
	if res != nil {
		for _, row := range res {
			attachments = append(attachments, &xmpp.Attachment{
				ImageURL:      row[1],
				ImageFilename: row[2],
				ThumbnailSize: row[3],
				ThumbnailURL:  row[4],
			})
		}
	}

	return attachments
}

func (c *Client) listen() {
	defer func() {
		if x := recover(); x != nil {
			log.Println("Closed with exception", x)
		}
	}()

	for {
		element, err := c.connection.Next()
		if err != nil {
			c.ClosedCh <- true
			c.Close()
			return
		}

		log.Printf("%s: %s", c.Id, element)

		switch element.Name.Local + element.Name.Space {
		case "iq" + xmpp.NsJabberClient: // rooms and rosters
			continue

			//query := c.connection.Query()
			//switch query.XMLName.Space {
			//case xmpp.NsMucRoom:
			//	items := make([]*Room, len(query.Items))
			//	for i, item := range query.Items {
			//		items[i] = &Room{Id: item.Jid, Name: item.Name,
			//			Owner: item.Owner, Topic: item.Topic}
			//	}
			//	c.receivedRooms <- items
			//case xmpp.NsIqRoster:
			//	items := make([]*User, len(query.Items))
			//	for i, item := range query.Items {
			//		items[i] = &User{Id: item.Jid, Name: item.Name, MentionName: item.MentionName}
			//	}
			//	c.receivedUsers <- items
			//}
		case "message" + xmpp.NsJabberClient:
			m := c.connection.Message(&element)
			if m.Body != "" || len(m.Attachments) > 0 {
				if m.Body == "#attachment" {
					m.Body = ""
				}

				c.ReceivedMessage <- &Message{
					From:        m.From,
					To:          m.To,
					Body:        m.Body,
					Mid:         m.MID,
					Stamp:       strtotime(m.Delay.Stamp),
					Attachments: getAttachments(m.Attachments, m.HTMLBody.Body),
				}

			} else if m.Fin.Body != "" {
				c.messageBuffer[m.Fin.QueryId] <- nil

			} else if m.Invite != nil && m.Invite.From != "" {
				items := make([]*Room, 1)
				items[0] = &Room{Id: m.Invite.From, Topic: m.Invite.Reason}
				c.receivedRooms <- items
			} else if m.Result.Body != "" {
				forwarded := c.connection.ForwardedMessage(m.Result.Body)

				if forwarded.Message.Body == "#attachment" {
					forwarded.Message.Body = ""
				}

				c.messageBuffer[m.Result.QueryId] <- &Message{
					From:        forwarded.Message.From,
					To:          forwarded.Message.To,
					Body:        forwarded.Message.Body,
					Mid:         forwarded.Message.MID,
					Stamp:       strtotime(forwarded.Delay.Stamp),
					Attachments: getAttachments(forwarded.Message.Attachments, forwarded.Message.HTMLBody.Body),
				}
			}
		default:
			log.Println(element.Name.Local, element.Name.Space, element.Attr)
		}
	}
}

func id() string {
	b := make([]byte, 8)
	io.ReadFull(rand.Reader, b)
	return fmt.Sprintf("%x", b)
}
