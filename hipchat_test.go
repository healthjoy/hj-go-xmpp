package hipchat
import (
  "testing"
  "testing/iotest"
  "time"
  )

func Test_HandleDisconnect(t *testing.T) {
  Host = "xmpp-dev.healthjoy.com"
  client, err := NewClient("roman.shevtsov@xmpp-dev.healthjoy.com", "password", "blabla")
  if (err != nil) {
    t.Fatalf("Connect error: %s", err)
  }
  client.connection.SetIncomingReader(iotest.TimeoutReader(client.connection.Outgoing()));
  go client.listen()
  go client.KeepAlive()
  select {
     case <-client.Messages():
     t.Logf("Ok")
     case <-time.After(1*time.Second):
     t.Logf("OK")
  }
  /*if (client.IsAlive()) {
    t.Fatalf("Still alive")
  }*/
  select {
     case <-client.Messages():
      t.Logf("Ok")
     case <-time.After(10*time.Second):
      t.Fatalf("Timeout!!")
  }
}
